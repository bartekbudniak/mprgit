package domain;

import java.util.List;

public class Person {
	private List<Address> address;
	private List<Role> rola;
	private String imie;
	private String nazwisko;
	private String numerTelefonu;

	public String getImie() {
		return imie;
	}

	public void setImie(String imie) {
		this.imie = imie;
	}

	public String getNazwisko() {
		return nazwisko;
	}

	public void setNazwisko(String nazwisko) {
		this.nazwisko = nazwisko;
	}

	public String getNumerTelefonu() {
		return numerTelefonu;
	}

	public void setNumerTelefonu(String numerTelefonu) {
		this.numerTelefonu = numerTelefonu;
	}

	public List<Role> getRola() {
		return rola;
	}

	public void setRola(List<Role> rola) {
		this.rola = rola;
	}

	public List<Address> getAddress() {
		return address;
	}

	public void setAddress(List<Address> address) {
		this.address = address;
	}
}
